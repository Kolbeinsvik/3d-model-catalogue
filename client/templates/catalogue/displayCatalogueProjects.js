

Template.displayCatalogueProjects.helpers({
	projects: function() {
		var projects = [];
		
		// MOCK: project model
		projects.push({title: 'Kalkun', description: '', files: []});
		projects.push({title: 'Hest', description: 'A project description', files: [{filename: 'nolo.stl'}]});
		projects.push({title: 'Svin', description: 'A project short description', files: [{filename: 'yolo.stl'}, {filename: 'folonolo.gcode'}]});
		projects.push({title: 'Ørret', description: 'A project looooooong description', files: [{filename: 'kolomari.stl'}, {filename: 'bolostari.stl'}]});
		projects.push({title: 'Pingvin', description: 'A project description with øæå', files: [{filename: 'fabita.stl'}, {filename: 'manita.stl'}, {filename: 'patisara.gcode'}, {filename: 'nalamanasirat.gcode'}]});
		
		
		Session.set('catalogueProjects', projects);
		return projects;
	},
	numberOfProjects: function() {
		var projects = Session.get('catalogueProjects');
		return projects.length;
	}
});